## RCT API ROUTES

### Login

```
method: POST,
link: http://localhost:4004/login,
data: {
    "username": "Trevor",
    "password": "admin"
}
```

### Admin

#### ** Modules **

##### \* Get all modules

```
method: GET,
link: http://localhost:4004/admin/modules/
data: {}
```

##### \* Add module

```
method: POST,
link: http://localhost:4004/admin/modules/add_module
data: {
    "U_desc": "admin",
    "U_status": "Active",
    "U_created_by": "154151589"
}
```

##### \* Update module

```
method: PUT,
link: http://localhost:4004/admin/modules/update_module
data: {
    "Code": 1,
    "U_desc": "admin",
    "U_status": "Active",
    "U_updated_by": "154151589"
}
```

#### ** Roles **

##### \* Get all roles

```
method: GET,
link: http://localhost:4004/admin/roles/
data: {}
```

##### \* Add role

```
method: POST,
link: http://localhost:4004/admin/roles/add_role
data: {
    "U_name": "admin-admin",
    "U_status": "Active",
    "U_created_by": "154151589"
}
```

##### \* Update role

```
method: PUT,
link: http://localhost:4004/admin/roles/update_role
data: {
    "Code": 1,
    "U_desc": "admin-admin",
    "U_status": "Inactive",
    "U_updated_by": "154151589"
}
```

#### ** Actions **

##### \* Get all actions

```
method: GET,
link: http://localhost:4004/admin/actions/
data: {}
```

##### \* Add action

```
method: POST,
link: http://localhost:4004/admin/roles/add_action
data: {
    "U_desc": "View modules",
    "U_status": "Active",
    "U_module_id": 1,
    "U_created_by": "154151589"
}
```

##### \* Update action

```
method: PUT,
link: http://localhost:4004/admin/roles/update_action
data: {
    "Code": 1,
    "U_desc": "View modules (updated)",
    "U_module_id": 1,
    "U_status": "Inactive",
    "U_updated_by": "154151589"
}
```

#### ** Receipt Types **

##### \* Get all receipt types

```
method: GET,
link: http://localhost:4004/admin/receipt_types/
data: {}
```

##### \* Add receipt type

```
method: POST,
link: http://localhost:4004/admin/receipt_types/add_receipt_type
data: {
    "U_name": "receipt type 2",
    "U_type": "type 2",
    "U_effective_date": "09-06-2020",
    "U_created_by": "154151589",
    "U_updated_by": "154151589",
    "U_created_time": "9:40:00",
    "U_updated_time": "9:40:00",
    "U_created_date": "09-06-2020",
    "U_updated_date": "09-06-2020"
}
```

##### \* Update receipt type

```
method: PUT,
link: http://localhost:4004/admin/receipt_types/update_receipt_type
data: {
    "Code": "2",
    "U_name": "receipt type 2 asd",
    "U_type": "type 2",
    "U_effective_date": "09-06-2020",
    "U_updated_by": "154151589"
}
```

#### ** Access Rights **

##### \* Get all access rights

```
method: GET,
link: http://localhost:4004/admin/access_rights/
data: {}
```

##### \* Add access rights

```
method: POST,
link: http://localhost:4004/admin/access_rights/add_access_rights
data: {
    "U_role_id": "1",
    "actions": ["1", "2", "3"],
    "U_status": "Active",
    "U_created_by": "154151589",
}
```

#### ** Users **

##### \* Get all users

```
method: GET,
link: http://localhost:4004/admin/users/
data: {}
```

##### \* Add user

```
method: POST,
link: http://localhost:4004/admin/users/add_user
{
    "U_role_id": "1",
    "U_username": "john",
    "U_password": "admin",
    "U_status": "Active",
    "U_first_name": "John Rey",
    "U_middle_name": "Bee",
    "U_last_name": "Asong",
    "U_address": "Somewhere in",
    "U_city": "Gensan",
    "U_created_by": "154151589",
    "U_created_date": "2019-12-09",
    "U_created_time": "14:22:00",
    "U_updated_by": "154151589",
    "U_updated_date": "2019-12-09",
    "U_updated_time": "14:22:00"
}
```

##### \* Update user

```
method: PUT,
link: http://localhost:4004/admin/users/update_user
{
    "Code": "2",
    "U_role_id": "1",
    "U_username": "john",
    "U_password": "admin",
    "U_status": "Active",
    "U_first_name": "John Rey",
    "U_middle_name": "Bee",
    "U_last_name": "Asong",
    "U_address": "Somewhere in",
    "U_city": "Gensan",
    "U_updated_by": "154151589"
}
```

#### ** Receipt Status **

##### \* Get all receipt status

```
method: GET,
link: http://localhost:4004/transaction/receipt_status/
data: {}
```

##### \* Add receipt status

```
method: POST,
link: http://localhost:4004/transaction/receipt_status/add_receipt_status
data: {
    "U_name": "Voided",
    "U_created_by": "154151589",
    "U_updated_by": "154151589",
    "U_created_time": "9:40:00",
    "U_updated_time": "9:40:00",
    "U_created_date": "09-06-2020",
    "U_updated_date": "09-06-2020"
}
```

##### \* Update receipt status

```
method: PUT,
link: http://localhost:4004/transaction/receipt_status/update_receipt_status
data: {
	"Code": 1,
    "U_name": "Printed",
    "U_updated_by": "154151589"
}
```

#### ** Groups **

##### \* Get list of groups

```
method: GET,
link: http://localhost:4004/transaction/groups/
data: {}
```

##### \* Add group

```
method: POST,
link: http://localhost:4004/transaction/groups/add_group
data: {
    "U_name": "Group Name",
    "U_desc": "A group name",
	"U_status": "Active",
    "U_effective_date": "09-06-2020",
    "U_created_by": "154151589",
    "U_updated_by": "154151589",
    "U_created_time": "9:40:00",
    "U_updated_time": "9:40:00",
    "U_created_date": "09-06-2020",
    "U_updated_date": "09-06-2020"
}
```

##### \* Update group

```
method: PUT,
link: http://localhost:4004/transaction/groups/update_group
data: {
	"Code": "2",
    "U_name": "Group Name (Updated)",
	"U_desc": "A group name",
	"U_status": "Active",
    "U_effective_date": "09-06-2020",
    "U_updated_by": "154151589"
}
```

#### ** Subroups **

##### \* Get list of subgroups

```
method: GET,
link: http://localhost:4004/transaction/subgroups/
data: {}
```

##### \* Add subgroup

```
method: POST,
link: http://localhost:4004/transaction/subgroups/add_subgroup
data: {
    "U_name": "Subgroup name",
    "U_group_id": "1",
	"U_desc": "A subgroup name",
	"U_status": "Active",
    "U_effective_date": "09-06-2020",
    "U_created_by": "154151589",
    "U_updated_by": "154151589",
    "U_created_time": "9:40:00",
    "U_updated_time": "9:40:00",
    "U_created_date": "09-06-2020",
    "U_updated_date": "09-06-2020"
}
```

##### \* Update subgroup

```
method: PUT,
link: http://localhost:4004/transaction/subgroups/update_subgroup
data: {
	"Code": "2",
    "U_name": "Subgroup name (updated)",
    "U_group_id": "1",
	"U_desc": "A subgroup name",
	"U_status": "Active",
    "U_effective_date": "09-06-2020",
    "U_updated_by": "154151589"
}
```

### Transaction

#### ** Transaction Head **

##### \* Get all transaction head

```
method: GET,
link: http://localhost:4004/transaction/transaction_head/
data: {}
```

##### \* Get receipt by status

```
method: GET,
link: http://localhost:4004/transaction/transaction_head/get_receipt_by_status
data: {
	"status": "Printed"
}
```

##### \* Add transaction head

```
method: POST,
link: http://localhost:4004/transaction/transaction_head/add_transaction_head
data: {
    "Name": "transaction at 15:12:12",
    "U_date": "09-06-2020",
    "U_series_id": "12",
    "U_organization_id": "1",
    "U_subgroup_id": "1",
    "U_tran_type_id": "1",
    "U_receipt_type_id": "1",
    "U_group_type_id": "1",
    "U_customer_name": "1",
    "U_paid_by": "Ceazar Masula",
    "U_remarks": "Remarks",
    "U_reviewed_by_id": "1",
    "U_status_id": "1",
    "U_tendered_type": "Tendered type",
    "U_amount": 5400,
    "U_reference_no": "03-PI-8126172",
    "U_official_receipt": "OR-245-124-125",
    "U_created_by": "154151589",
    "U_updated_by": "154151589",
    "U_created_time": "9:40:00",
    "U_updated_time": "9:40:00",
    "U_created_date": "09-06-2020",
    "U_updated_date": "09-06-2020"
}
```

##### \* Update transaction head

```
method: PUT,
link: http://localhost:4004/transaction/transaction_head/update_transaction_head
data: {
    "Code": "3",
    "Name": "transaction at 15:12:12",
    "U_date": "09-06-2020",
    "U_series_id": "12",
    "U_organization_id": "1",
    "U_subgroup_id": "1",
    "U_tran_type_id": "1",
    "U_receipt_type_id": "1",
    "U_group_type_id": "1",
    "U_customer_name": "1",
    "U_paid_by": "Ceazar Masula",
    "U_remarks": "Remarks",
    "U_reviewed_by_id": "1",
    "U_status_id": "1",
    "U_tendered_type": "Tendered type",
    "U_amount": 5400,
    "U_reference_no": "03-PI-8126172",
    "U_official_receipt": "OR-245-124-125",
    "U_updated_by": "154151589"
}
```
