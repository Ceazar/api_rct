const axios = require("axios");
const https = require("https");

const axiosRequest = async ({ method, link, headers, data }) => {
    return await axios({
        method: method,
        url: `${process.env.SL_URL}${link}`,
        headers,
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        data
    });
};

module.exports = { axiosRequest };
