module.exports = function buildGetReceiptByStatus() {
    return async function getReceiptByStatus(request_info, transactionDB, SessionId) {
        const {
            status
        } = request_info

        // Validations
        if (!status) {
            throw new Error("Status is required.")
        }

        const exists = await transactionDB.getReceiptStatusByName(status, SessionId);

        if (!exists) {
            throw new Error("Status doesn't exists.");
        }

        return Object.freeze({
            getStatusId: () => exists.value[0].Code
        })
    }
}