module.exports = function buildAddTranHead() {
    return async function addTranHead(request_info, transactionDB, SessionId) {
        const {
            Name,
            U_series_id,
            U_organization_id,
            U_subgroup_id,
            U_tran_type_id,
            U_receipt_type_id,
            U_group_type_id,
            U_customer_name,
            U_status_id,
            U_tendered_type,
            U_amount,
            U_reference_no,
            U_official_receipt,
            U_created_by
        } = request_info

        // Validations
        if (!Name) {
            throw new Error("Name is required.")
        }
        if (!U_series_id) {
            throw new Error("Series ID is required.")
        }
        if (!U_organization_id) {
            throw new Error("Organization ID is required.")
        }
        if (!U_subgroup_id) {
            throw new Error("Subgroup ID is required.")
        }
        if (!U_tran_type_id) {
            throw new Error("Transaction type is required.")
        }
        if (!U_receipt_type_id) {
            throw new Error("Receipt type is required.")
        }
        if (!U_group_type_id) {
            throw new Error("Group type is required.")
        }
        if (!U_customer_name) {
            throw new Error("Customer Name is required.")
        }
        if (!U_status_id) {
            throw new Error("Status ID is required.")
        }
        if (!U_tendered_type) {
            throw new Error("Tendered type is required.")
        }
        if (!U_amount) {
            throw new Error("Amount is required.")
        }
        if (!U_reference_no) {
            throw new Error("Reference Number is required.")
        }
        if (!U_official_receipt) {
            throw new Error("Official Receipt is required.")
        }
        if (!U_created_by) {
            throw new Error("Created by is required.");
        }
    }
}