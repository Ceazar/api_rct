const buildAddTransactioHead = require("./add_transaction_head")
const buildUpdateTransactioHead = require("./edit_transaction_head");
const buildGetReceiptByStatus = require("./get_receipt_by_status");

const makeAddTransactioHead = buildAddTransactioHead();
const makeUpdateTransactioHead = buildUpdateTransactioHead();
const makeGetReceiptByStatus = buildGetReceiptByStatus();

// Export entity validations
module.exports = ({
    makeAddTransactioHead,
    makeUpdateTransactioHead,
    makeGetReceiptByStatus
})