const buildAddReceiptStatus = require("./add_receipt_status")
const buildUpdateReceiptStatus = require("./edit_receipt_status");

const makeAddReceiptStatus = buildAddReceiptStatus();
const makeUpdateReceiptStatus = buildUpdateReceiptStatus();

// Export entity validations
module.exports = ({
    makeAddReceiptStatus,
    makeUpdateReceiptStatus
})