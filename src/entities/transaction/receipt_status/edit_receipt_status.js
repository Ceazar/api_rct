module.exports = function buildUpdateReceiptStatus() {
    return async function updateReceiptStatus(request_info, transactionDB, SessionId) {
        const {
            Code,
            U_name,
            U_updated_by
        } = request_info

        // Validations
        if (!Code) {
            throw new Error("ID is required.")
        }
        if (!U_name) {
            throw new Error("Name is required.")
        }

        const exists = await transactionDB.getReceiptStatusByID(Code, SessionId);

        if (!exists) {
            throw new Error("Status does not exists.");
        }

        if (!U_updated_by) {
            throw new Error("Updated by is required.");
        }
    }
}