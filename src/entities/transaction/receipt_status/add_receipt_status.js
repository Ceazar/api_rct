module.exports = function buildAddReceiptStatus() {
    return async function addReceiptStatus(request_info, transactionDB, SessionId) {
        const {
            U_name,
            U_created_by
        } = request_info

        // Validations
        if (!U_name) {
            throw new Error("Name is required.")
        }

        const exists = await transactionDB.getReceiptStatusByName(U_name, SessionId);

        if (exists.value.length != 0) {
            throw new Error("Status already exists.");
        }

        if (!U_created_by) {
            throw new Error("Created by is required.");
        }
    }
}