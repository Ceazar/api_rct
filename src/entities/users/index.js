const buildLogin = require("./login");

const makeUserLogin = buildLogin();

module.exports = {
    makeUserLogin
}