module.exports = function buildAction() {
    return async function makeLogin(request_info) {
        const {
            username,
            password
        } = request_info;

        // Check if username is not null
        if (!username) {
            throw new Error("Username is required.")
        }
        // Check if password is not null
        if (!password) {
            throw new Error("Password is required.")
        }
    }
}