const buildAddUser = require("./add_user")
const buildUpdateUser = require("./edit_user");

const makeAddUser = buildAddUser();
const makeUpdateUser = buildUpdateUser();

// Export entity validations
module.exports = ({
    makeAddUser,
    makeUpdateUser
})