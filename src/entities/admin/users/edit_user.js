module.exports = function buildUpdateUser() {
    return async function updateUser(request_info, adminDB, SessionId) {
        const {
            Code,
            U_first_name,
            U_middle_name,
            U_last_name,
            U_role_id,
            U_created_by
        } = request_info

        // Validations
        if (!Code) {
            throw new Error("ID is required.");
        }
        if (!U_first_name) {
            throw new Error("First name is required.");
        }
        if (!U_middle_name) {
            throw new Error("Middle name is required.");
        }
        if (!U_last_name) {
            throw new Error("Last name is required.");
        }
        if (!U_role_id) {
            throw new Error("Role is required.");
        }
        if (!U_created_by) {
            throw new Error("Created by is required.");
        }

        const exists = await adminDB.findUserById(Code, SessionId);

        if (exists.value.length == 0) {
            throw new Error("User does not exists.");
        }

        // return Object.freeze({
        //     getDescription: () => U_desc,
        //     getStatus: () => U_status,
        //     getCreatedBy: () => U_created_by
        // })
    }
}