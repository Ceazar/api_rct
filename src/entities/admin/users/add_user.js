module.exports = function buildAddUser() {
    return async function addUser(request_info, adminDB, SessionId) {
        const {
            U_first_name,
            U_middle_name,
            U_last_name,
            U_role_id,
            U_username,
            U_password,
            U_created_by
        } = request_info

        // Validations
        if (!U_first_name) {
            throw new Error("First name is required.");
        }
        if (!U_middle_name) {
            throw new Error("Middle name is required.");
        }
        if (!U_last_name) {
            throw new Error("Last name is required.");
        }
        if (!U_role_id) {
            throw new Error("Role is required.");
        }
        if (!U_username) {
            throw new Error("Username is required.");
        }
        if (!U_password) {
            throw new Error("Password is required.");
        }
        if (!U_created_by) {
            throw new Error("Created by is required.");
        }

        const exists = await adminDB.findUserByName(request_info, SessionId);

        if (exists.value.length != 0) {
            throw new Error("User already exists.");
        }

        // return Object.freeze({
        //     getDescription: () => U_desc,
        //     getStatus: () => U_status,
        //     getCreatedBy: () => U_created_by
        // })
    }
}