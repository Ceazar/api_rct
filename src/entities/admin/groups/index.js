const buildAddGroup = require("./add_group")
const buildUpdateGroup = require("./edit_group");

const makeAddGroup = buildAddGroup();
const makeUpdateGroup = buildUpdateGroup();

// Export entity validations
module.exports = ({
    makeAddGroup,
    makeUpdateGroup
})