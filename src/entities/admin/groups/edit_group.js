module.exports = function buildUpdateGroup() {
    return async function updateGroup(request_info, adminDB, SessionId) {
        const {
            Code,
            U_name,
            U_desc,
            U_effective_date,
            U_status,
            U_updated_by
        } = request_info

        // Validations
        if (!U_name) {
            throw new Error("Name is required.");
        }
        if (!U_desc) {
            throw new Error("Description is required.");
        }
        if (!U_status) {
            throw new Error("Status is required.");
        }
        if (!U_effective_date) {
            throw new Error("Effective date is required.")
        }
        if (!U_updated_by) {
            throw new Error("Updated by is required.");
        }

        const exists = await adminDB.findGroupByID(Code, SessionId);

        if (!exists) {
            throw new Error("Group doesn't exists.");
        }

        // return Object.freeze({
        //     getDescription: () => U_desc,
        //     getStatus: () => U_status,
        //     getCreatedBy: () => U_created_by
        // })
    }
}