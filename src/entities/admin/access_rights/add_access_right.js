module.exports = function buildAddAccessRights() {
    return async function addAccessRights(request_info) {
        const {
            U_role_id,
            actions,
            U_created_by
        } = request_info

        // Validations
        if (!U_role_id) {
            throw new Error("Role ID is required.");
        }
        if (!actions) {
            console.log('Empty actions.');
        }
        if (!U_created_by) {
            throw new Error("Created by is required.")
        }

        // return Object.freeze({
        //     getDescription: () => U_desc,
        //     getStatus: () => U_status,
        //     getCreatedBy: () => U_created_by
        // })
    }
}