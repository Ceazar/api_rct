const buildAddAccessRights = require("./add_access_right")

const makeAddAR = buildAddAccessRights();

// Export entity validations
module.exports = ({
    makeAddAR
})