const buildAddReceiptType = require("./add_receipt_types")
const buildUpdateReceiptType = require("./edit_receipt_types");

const makeAddReceiptType = buildAddReceiptType();
const makeUpdateReceiptType = buildUpdateReceiptType();

// Export entity validations
module.exports = ({
    makeAddReceiptType,
    makeUpdateReceiptType
})