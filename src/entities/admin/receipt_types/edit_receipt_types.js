module.exports = function buildUpdateReceiptType() {
    return async function updateReceiptType(request_info, adminDB, SessionId) {
        const {
            Code,
            U_name,
            U_type,
            U_effective_date,
            U_updated_by
        } = request_info

        // Validations
        if (!Code) {
            throw new Error("Code is required.");
        }
        if (!U_name) {
            throw new Error("Name is required.");
        }
        if (!U_type) {
            throw new Error("Type is required.");
        }
        if (!U_effective_date) {
            throw new Error("Effective date is required.")
        }
        if (!U_updated_by) {
            throw new Error("Updated by is required.");
        }

        const exists = await adminDB.findReceiptTypeByID(Code, SessionId);

        if (!exists) {
            throw new Error("Receipt Type doesn't exists.");
        }

        // return Object.freeze({
        //     getDescription: () => U_desc,
        //     getStatus: () => U_status,
        //     getCreatedBy: () => U_created_by
        // })
    }
}