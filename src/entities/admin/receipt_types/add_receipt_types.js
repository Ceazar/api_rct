module.exports = function buildAddReceiptType() {
    return async function addReceiptType(request_info, adminDB, SessionId) {
        const {
            U_name,
            U_type,
            U_effective_date,
            U_created_by
        } = request_info

        // Validations
        if (!U_name) {
            throw new Error("Name is required.");
        }
        if (!U_type) {
            throw new Error("Type is required.");
        }
        if (!U_effective_date) {
            throw new Error("Effective date is required.")
        }
        if (!U_created_by) {
            throw new Error("Created by is required.");
        }

        const exists = await adminDB.findReceiptTypeByName(U_name, SessionId);

        if (exists.value.length != 0) {
            throw new Error("ReceiptType already exists.");
        }

        // return Object.freeze({
        //     getDescription: () => U_desc,
        //     getStatus: () => U_status,
        //     getCreatedBy: () => U_created_by
        // })
    }
}