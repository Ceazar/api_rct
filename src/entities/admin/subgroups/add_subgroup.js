module.exports = function buildAddSubgroup() {
    return async function addSubgroup(request_info, adminDB, SessionId) {
        const {
            U_name,
            U_desc,
            U_group_id,
            U_effective_date,
            U_status,
            U_created_by
        } = request_info

        // Validations
        if (!U_name) {
            throw new Error("Name is required.");
        }
        if (!U_desc) {
            throw new Error("Description is required.");
        }
        if (!U_status) {
            throw new Error("Status is required.");
        }
        if (!U_group_id) {
            throw new Error("Subgroup ID is required.");
        }
        if (!U_effective_date) {
            throw new Error("Effective date is required.")
        }
        if (!U_created_by) {
            throw new Error("Created by is required.");
        }

        const exists = await adminDB.findSubgroupByName(U_name, SessionId);

        if (exists.value.length != 0) {
            throw new Error(`Subgroup ${U_name} already exists.`);
        }

        // return Object.freeze({
        //     getDescription: () => U_desc,
        //     getStatus: () => U_status,
        //     getCreatedBy: () => U_created_by
        // })
    }
}