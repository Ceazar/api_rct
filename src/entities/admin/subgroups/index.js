const buildAddSubgroup = require("./add_subgroup")
const buildUpdateSubgroup = require("./edit_subgroup");

const makeAddSubgroup = buildAddSubgroup();
const makeUpdateSubgroup = buildUpdateSubgroup();

// Export entity validations
module.exports = ({
    makeAddSubgroup,
    makeUpdateSubgroup
})