module.exports = function buildAddAction() {
    return async function addAction(request_info, adminDB, SessionId) {
        const {
            U_desc,
            U_status,
            U_module_id,
            U_created_by
        } = request_info

        // Validations
        if (!U_status) {
            throw new Error("Status is required.");
        }
        if (!U_desc) {
            throw new Error("Description is required.");
        }
        if (!U_module_id) {
            throw new Error("Module ID is required.")
        }
        if (!U_created_by) {
            throw new Error("Created by is required.");
        }

        const exists = await adminDB.findActionByDesc(U_desc, SessionId);

        if (exists.value.length != 0) {
            throw new Error("Action already exists.");
        }

        // return Object.freeze({
        //     getDescription: () => U_desc,
        //     getStatus: () => U_status,
        //     getCreatedBy: () => U_created_by
        // })
    }
}