const buildAddAction = require("./add_action")
const buildUpdateAction = require("./edit_action");

const makeAddAction = buildAddAction();
const makeUpdateAction = buildUpdateAction();

// Export entity validations
module.exports = ({
    makeAddAction,
    makeUpdateAction
})