const buildAddModule = require("./add_module")
const buildUpdateModule = require("./edit_module");

const makeAddModule = buildAddModule();
const makeUpdateModule = buildUpdateModule();

// Export entity validations
module.exports = ({
    makeAddModule,
    makeUpdateModule
})