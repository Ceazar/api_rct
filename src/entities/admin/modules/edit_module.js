module.exports = function buildUpdateModule() {
    return async function updateModule(request_info, adminDB, SessionId) {
        const {
            Code,
            U_desc,
            U_status,
            U_updated_by
        } = request_info

        // Validations
        if (!Code) {
            throw new Error("Module ID is required.");
        }
        if (!U_status) {
            throw new Error("Status is required.");
        }
        if (!U_desc) {
            throw new Error("Description is required.");
        }
        if (!U_updated_by) {
            throw new Error("Updated by is required.");
        }

        const exists = await adminDB.findModuleByID(Code, SessionId);

        if (exists.rowCount == 0) {
            throw new Error("Module does not exists.");
        }
    }
}