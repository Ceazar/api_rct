module.exports = function buildAddModule() {
    return async function addModule(request_info, adminDB, SessionId) {
        const {
            U_desc,
            U_status,
            U_created_by
        } = request_info

        // Validations
        if (!U_status) {
            throw new Error("Status is required.");
        }
        if (!U_desc) {
            throw new Error("Description is required.");
        }
        if (!U_created_by) {
            throw new Error("Created by is required.");
        }

        const exists = await adminDB.findModuleByDesc(U_desc, SessionId);

        if (exists.value.length != 0) {
            throw new Error("Module already exists.");
        }
        // return Object.freeze({
        //     getDescription: () => U_desc,
        //     getStatus: () => U_status,
        //     getCreatedBy: () => U_created_by
        // })
    }
}