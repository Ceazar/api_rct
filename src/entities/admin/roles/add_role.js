module.exports = function buildAddRole() {
    return async function addRole(request_info, adminDB, SessionId) {
        const {
            U_name,
            U_status,
            U_created_by
        } = request_info

        // Validations
        if (!U_status) {
            throw new Error("Status is required.");
        }
        if (!U_name) {
            throw new Error("Name is required.");
        }
        if (!U_created_by) {
            throw new Error("Created by is required.");
        }

        const exists = await adminDB.findRoleByDesc(U_name, SessionId);

        if (exists.value.length != 0) {
            throw new Error("Role already exists.");
        }

        // return Object.freeze({
        //     getDescription: () => U_desc,
        //     getStatus: () => U_status,
        //     getCreatedBy: () => U_created_by
        // })
    }
}