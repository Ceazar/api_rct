module.exports = function buildUpdateRole() {
    return async function updateRole(request_info, adminDB, SessionId) {
        const {
            Code,
            U_name,
            U_status,
            U_updated_by
        } = request_info

        // Validations
        if (!Code) {
            throw new Error("Role ID is required.");
        }
        if (!U_status) {
            throw new Error("Status is required.");
        }
        if (!U_name) {
            throw new Error("Name is required.");
        }
        if (!U_updated_by) {
            throw new Error("Updated by is required.");
        }

        const exists = await adminDB.findModuleByID(Code, SessionId);

        if (exists.value.length == 0) {
            throw new Error("Role does not exists.");
        }
    }
}