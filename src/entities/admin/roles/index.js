const buildAddRole = require("./add_role")
const buildUpdateRole = require("./edit_role");

const makeAddRole = buildAddRole();
const makeUpdateRole = buildUpdateRole();

// Export entity validations
module.exports = ({
    makeAddRole,
    makeUpdateRole
})