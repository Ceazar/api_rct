const jwt = require("jsonwebtoken");

async function createToken(username) {
  return jwt.sign({ username }, "s3Cr3tP@ssW0rD", {
    expiresIn: "8h"
  });
}

async function authorization(req, res, next) {
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    const bearer = bearerHeader.split(" ");
    const bearerToken = bearer[1];
    req.token = bearerToken;
    next();
  } else {
    res.sendStatus(403);
  }
}
async function verifyToken(token, key) {
  return jwt.verify(token, key, async err => {
    if (err) {
      return false;
    } else {
      return true;
    }
  });
}

module.exports = {
  createToken,
  authorization,
  verifyToken
};
