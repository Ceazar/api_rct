module.exports = function adminDB({ moment }, { axiosRequest }, { https }) {
    return Object.freeze({
        // Login
        SAPlogin,

        // Modules
        getAllModules,
        findModuleByDesc,
        findModuleByID,
        addModule,
        updateModule,

        // Roles
        getAllRoles,
        findRoleByDesc,
        findRoleByID,
        addRole,
        updateRole,

        // Actions
        getAllActions,
        findActionByDesc,
        findActionByID,
        addAction,
        updateAction,

        // Access Rights
        getAllAccessRights,
        findARExist,
        addAccessRights,
        removeAccessRights,
        getARPerRole,
        patchAccessRights,

        // Users
        getAllUsers,
        findUserByName,
        findUserById,
        addUser,
        updateUser,

        // RECEIPT_TYPES
        getReceiptType,
        findReceiptTypeByID,
        findReceiptTypeByName,
        addReceiptType,
        updateReceiptType,

        // Groups
        getGroups,
        findGroupByID,
        findGroupByName,
        addGroup,
        updateGroup,

        // Subgroups
        getSubgroups,
        findSubgroupByID,
        findSubgroupByName,
        addSubgroup,
        updateSubgroup
    });

    // Login
    async function SAPlogin() {
        return await axiosRequest({
            method: "POST",
            link: "/b1s/v1/Login",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                CompanyDB: process.env.SL_CompanyDB,
                Password: process.env.SL_Password,
                UserName: process.env.SL_UserName
            }
        })
            .then(res => {
                return res.data.SessionId
            })
            .catch(err => {
                if (err.code == "ECONNREFUSED") {
                    throw new Error("Network Error!");
                } else if (!err.response) throw new Error(err.message);
                else if (err.response.status === 502) {
                    throw new Error(err.response.statusText);
                } else if (err.response.data.error) {
                    throw new Error(err.response.data.error.message.value);
                } else {
                    throw new Error(err.response.data);
                }
            });
    }

    // Subgroups
    async function getSubgroups(SessionId) { // Get Subgroups
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_SUBGROUPS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findSubgroupByID(Code, SessionId) { // Get subgroup by ID
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_SUBGROUPS('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findSubgroupByName(U_name, SessionId) { // Get subgroup by name
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_SUBGROUPS?$filter=U_name eq '${U_name}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function addSubgroup(request_info, SessionId) { // add subgroup
        const {
            U_name,
            U_desc,
            U_group_id,
            U_effective_date,
            U_status,
            U_created_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Get Code
        let codeMax = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_SUBGROUPS/$count`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
        }).then(res => {
            return res.data
        }).catch(e => {
            throw new Error(e)
        })

        codeMax = codeMax + 1;

        // Query
        return await axiosRequest({
            method: "POST",
            link: `/b1s/v1/U_SUBGROUPS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Code: codeMax,
                Name: U_name,
                U_name,
                U_group_id,
                U_desc,
                U_effective_date,
                U_status,
                U_created_by,
                U_updated_by: U_created_by,
                U_created_time: time_today,
                U_updated_time: time_today,
                U_created_date: date_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function updateSubgroup(request_info, SessionId) { // update subgroup
        const {
            Code,
            U_name,
            U_desc,
            U_group_id,
            U_effective_date,
            U_status,
            U_updated_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");


        // Query
        return await axiosRequest({
            method: "PATCH",
            link: `/b1s/v1/U_SUBGROUPS('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Name: U_name,
                U_name,
                U_group_id,
                U_desc,
                U_effective_date,
                U_status,
                U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }



    // Groups
    async function getGroups(SessionId) { // Get groups
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_GROUPS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findGroupByID(Code, SessionId) { // Get group by id
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_GROUPS('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findGroupByName(U_name, SessionId) { // find group by name
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_GROUPS?$filter=U_name eq '${U_name}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function addGroup(request_info, SessionId) { // add group
        const {
            U_name,
            U_desc,
            U_effective_date,
            U_status,
            U_created_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Get Code
        let codeMax = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_GROUPS/$count`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
        }).then(res => {
            return res.data
        }).catch(e => {
            throw new Error(e)
        })

        codeMax = codeMax + 1;

        // Query
        return await axiosRequest({
            method: "POST",
            link: `/b1s/v1/U_GROUPS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Code: codeMax,
                Name: U_name,
                U_name,
                U_desc,
                U_effective_date,
                U_status,
                U_created_by,
                U_updated_by: U_created_by,
                U_created_time: time_today,
                U_updated_time: time_today,
                U_created_date: date_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function updateGroup(request_info, SessionId) { // update group
        const {
            Code,
            U_name,
            U_desc,
            U_effective_date,
            U_status,
            U_updated_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");


        // Query
        return await axiosRequest({
            method: "PATCH",
            link: `/b1s/v1/U_GROUPS('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Name: U_name,
                U_name,
                U_desc,
                U_effective_date,
                U_status,
                U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    // Receipt types
    async function getReceiptType(SessionId) { // Get receipt type
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_RECEIPT_TYPES`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findReceiptTypeByID(Code, SessionId) { // Get receipt type
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_RECEIPT_TYPES('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findReceiptTypeByName(U_name, SessionId) { // Get receipt type
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_RECEIPT_TYPES?$filter=U_name eq '${U_name}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function addReceiptType(request_info, SessionId) { // Get receipt type
        const {
            U_name,
            U_type,
            U_effective_date,
            U_created_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Get Code
        let codeMax = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_RECEIPT_TYPES/$count`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
        }).then(res => {
            return res.data
        }).catch(e => {
            throw new Error(e)
        })

        codeMax = codeMax + 1;

        // Query
        return await axiosRequest({
            method: "POST",
            link: `/b1s/v1/U_RECEIPT_TYPES`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Code: codeMax,
                Name: U_name,
                U_name,
                U_type,
                U_effective_date,
                U_created_by,
                U_updated_by: U_created_by,
                U_created_time: time_today,
                U_updated_time: time_today,
                U_created_date: date_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function updateReceiptType(request_info, SessionId) { // update receipt type
        const {
            Code,
            U_name,
            U_type,
            U_effective_date,
            U_updated_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");


        // Query
        return await axiosRequest({
            method: "PATCH",
            link: `/b1s/v1/U_RECEIPT_TYPES('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Name: U_name,
                U_name,
                U_type,
                U_effective_date,
                U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }


    // Users
    async function getAllUsers(SessionId) { // Get all users
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_USERS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findUserByName(request_info, SessionId) { // Find user by name
        const { U_first_name, U_middle_name, U_last_name } = request_info

        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_USERS?$filter=U_first_name eq '${U_first_name}' and U_middle_name eq '${U_middle_name}' and U_last_name eq '${U_last_name}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findUserById(Code, SessionId) { // Find user by ID

        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_USERS?$filter=Code eq '${Code}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function addUser(request_info, SessionId) { // Add user
        const {
            U_first_name,
            U_middle_name,
            U_last_name,
            U_role_id,
            U_username,
            U_password,
            U_address,
            U_city,
            U_created_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Get Code
        let codeMax = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_USERS/$count`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
        }).then(res => {
            return res.data
        }).catch(e => {
            throw new Error(e)
        })

        codeMax = codeMax + 1;
        const Name = U_first_name + " " + U_middle_name + " " + U_last_name;

        return await axiosRequest({
            method: "POST",
            link: `/b1s/v1/U_USERS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Code: codeMax,
                Name,
                U_first_name,
                U_middle_name,
                U_last_name,
                U_role_id,
                U_status: "Active",
                U_username,
                U_password,
                U_address,
                U_city,
                U_created_by,
                U_updated_by: U_created_by,
                U_created_time: time_today,
                U_updated_time: time_today,
                U_created_date: date_today,
                U_updated_date: date_today

            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function updateUser(request_info, SessionId) { // Add user
        const {
            Code,
            U_first_name,
            U_middle_name,
            U_last_name,
            U_role_id,
            U_username,
            U_password,
            U_address,
            U_city,
            U_updated_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        let Name;

        if (U_first_name && U_middle_name && U_last_name) {
            Name = U_first_name + " " + U_middle_name + " " + U_last_name;
        }

        return await axiosRequest({
            method: "PATCH",
            link: `/b1s/v1/U_USERS('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Name,
                U_first_name,
                U_middle_name,
                U_last_name,
                U_role_id,
                U_status: "Active",
                U_username,
                U_password,
                U_address,
                U_city,
                U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today

            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    // Access Rights
    async function getAllAccessRights(SessionId) { // Get accessRights
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_ACCESS_RIGHTS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function getARPerRole(U_role_id, SessionId) { // Get Actions of role
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/$crossjoin(U_ACCESS_RIGHTS,U_ACTIONS)?$expand=U_ACCESS_RIGHTS($select=Code,Name,U_action_id,U_role_id,U_status),U_ACTIONS($select=Code,Name,U_module_id,U_desc,U_status)&$filter=U_ACCESS_RIGHTS/U_action_id eq U_ACTIONS/Code and U_ACCESS_RIGHTS/U_role_id eq '${U_role_id}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findARExist(U_role_id, U_action_id) { // Get actions of role
        return pool.query(`SELECT * FROM access_rights 
            WHERE "U_role_id" = $1 and "U_action_id" = $2`, [U_role_id, U_action_id])
    }

    async function addAccessRights(U_role_id, U_action_id, U_created_by, SessionId) { // Add access rights
        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Get Code
        let codeMax = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_ACCESS_RIGHTS/$count`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
        }).then(res => {
            return res.data
        }).catch(e => {
            throw new Error(e)
        })

        codeMax = codeMax + 1;

        // Query
        return await axiosRequest({
            method: "POST",
            link: `/b1s/v1/U_ACCESS_RIGHTS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Code: codeMax,
                Name: codeMax,
                U_action_id,
                U_role_id,
                U_status: "Active",
                U_created_by,
                U_updated_by: U_created_by,
                U_created_time: time_today,
                U_updated_time: time_today,
                U_created_date: date_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function patchAccessRights(Code, U_status, U_created_by, SessionId, ) { // Edit access rights
        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Query
        return await axiosRequest({
            method: "PATCH",
            link: `/b1s/v1/U_ACCESS_RIGHTS('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                U_status,
                U_updated_by: U_created_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function removeAccessRights(request_info) { // Remove access right of role
        const {
            U_role_id,
            actions
        } = request_info

        // Query
        return pool.query(`DELETE FROM access_rights WHERE "U_role_id" = $1 and
        "U_action_id in ($2)"`, [U_role_id, actions]);
    }

    // Actions
    async function getAllActions(SessionId) { // Get actions
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_ACTIONS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findActionByDesc(U_desc, SessionId) { // Check existing data
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_ACTIONS?$filter=U_desc eq '${U_desc}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findActionByID(Code, SessionId) { // Check existing data
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_ACTIONS?$filter=Code eq '${Code}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function addAction(request_info, SessionId) { // Add action
        const {
            U_desc,
            U_status,
            U_module_id,
            U_created_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Get Code
        let codeMax = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_ACTIONS/$count`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
        }).then(res => {
            return res.data
        }).catch(e => {
            throw new Error(e)
        })

        codeMax = codeMax + 1;

        // Query
        return await axiosRequest({
            method: "POST",
            link: `/b1s/v1/U_ACTIONS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Code: codeMax,
                Name: U_desc,
                U_desc,
                U_status,
                U_module_id,
                U_created_by,
                U_updated_by: U_created_by,
                U_created_time: time_today,
                U_updated_time: time_today,
                U_created_date: date_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function updateAction(request_info, SessionId) { // Update action
        const {
            Code,
            U_desc,
            U_module_id,
            U_status,
            U_updated_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Query
        return await axiosRequest({
            method: "PATCH",
            link: `/b1s/v1/U_ACTIONS('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Name: U_desc,
                U_desc,
                U_module_id,
                U_status,
                U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return {
                Name: U_desc,
                U_desc,
                U_module_id,
                U_status,
                U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    // Roles 
    async function getAllRoles(SessionId) { // Get roles
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_ROLES`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findRoleByDesc(U_name, SessionId) { // Check existing data
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_ROLES?$filter=U_name eq '${U_name}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findRoleByID(Code, SessionId) { // Check existing data
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_ROLES?$filter=Code eq '${Code}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function addRole(request_info, SessionId) { // Add role
        const {
            U_name,
            U_status,
            U_created_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Get Code
        let codeMax = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_ROLES/$count`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
        }).then(res => {
            return res.data
        }).catch(e => {
            throw new Error(e)
        })

        codeMax = codeMax + 1;

        // Query
        return await axiosRequest({
            method: "POST",
            link: `/b1s/v1/U_ROLES`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Code: codeMax,
                Name: U_name,
                U_name,
                U_status,
                U_created_by,
                U_updated_by: U_created_by,
                U_created_time: time_today,
                U_updated_time: time_today,
                U_created_date: date_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function updateRole(request_info, SessionId) { // Update role
        const {
            Code,
            U_name,
            U_status,
            U_updated_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Query
        return await axiosRequest({
            method: "PATCH",
            link: `/b1s/v1/U_ROLES('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Name: U_name,
                U_name,
                U_status,
                U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return {
                Name: U_name,
                U_name,
                U_status,
                U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    // Modules
    async function getAllModules(SessionId) { // Get modules
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_MODULES`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findModuleByDesc(U_desc, SessionId) { // Check existing data
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_MODULES?$filter=U_desc eq '${U_desc}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function findModuleByID(Code, SessionId) { // Check existing data
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_MODULES?$filter=Code eq '${Code}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function addModule(request_info, SessionId) { // Add Module
        const {
            U_desc,
            U_status,
            U_created_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Get Code
        let codeMax = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_MODULES/$count`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
        }).then(res => {
            return res.data
        }).catch(e => {
            throw new Error(e)
        })

        codeMax = codeMax + 1;

        // Query
        return await axiosRequest({
            method: "POST",
            link: `/b1s/v1/U_MODULES`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Code: codeMax,
                Name: U_desc,
                U_desc,
                U_status,
                U_created_by,
                U_updated_by: U_created_by,
                U_created_time: time_today,
                U_updated_time: time_today,
                U_created_date: date_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function updateModule(request_info, SessionId) { // Update module
        const {
            Code,
            U_desc,
            U_status,
            U_updated_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Query
        return await axiosRequest({
            method: "PATCH",
            link: `/b1s/v1/U_MODULES('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Name: U_desc,
                U_desc,
                U_status,
                U_updated_by: U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return {
                Code,
                Name: U_desc,
                U_desc,
                U_status,
                U_updated_by: U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }
}