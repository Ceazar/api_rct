const { axiosRequest } = require("../../axios/index");

// Moment
const moment = require("moment-timezone");
const admin = require("./admin");

require("tls").DEFAULT_MIN_VERSION = "TLSv1";
var https = require("https");

const adminDB = admin({ moment }, { axiosRequest }, { https });

module.exports = adminDB;
