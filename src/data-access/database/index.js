// var hdb = require('hdb');
// var client = hdb.createClient({
//     host: process.env.SAP_HOST,
//     port: process.env.SAP_PORT,
//     user: process.env.SAP_USERNAME,
//     password: process.env.SAP_PASSWORD
// });

// module.exports = client;

const Pool = require("pg").Pool;
const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "/../../config/config.json")[env];

const pool = new Pool(config);

module.exports = pool;