const { axiosRequest } = require("../../axios/index");

// Moment
const moment = require("moment-timezone");
const transaction = require("./transaction");

require("tls").DEFAULT_MIN_VERSION = "TLSv1";
var https = require("https");

const transactionDB = transaction({ moment }, { axiosRequest }, { https });

module.exports = transactionDB;
