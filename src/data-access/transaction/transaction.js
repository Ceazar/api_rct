module.exports = function transactionDB({ moment }, { axiosRequest }, { https }) {
    return Object.freeze({
        // Login
        SAPlogin,

        // Transaction Heads
        getTransactionHeads,
        getTransactionHeadsById,
        getTransactionHeadsByStatus,
        addTransactionHead,
        updateTransactionHead,

        // Receipt Status
        getReceiptStatus,
        getReceiptStatusByID,
        getReceiptStatusByName,
        addReceiptStatus,
        updateReceiptStatus

    });

    // Login
    async function SAPlogin() {
        return await axiosRequest({
            method: "POST",
            link: "/b1s/v1/Login",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                CompanyDB: process.env.SL_CompanyDB,
                Password: process.env.SL_Password,
                UserName: process.env.SL_UserName
            }
        })
            .then(res => {
                return res.data.SessionId
            })
            .catch(err => {
                if (err.code == "ECONNREFUSED") {
                    throw new Error("Network Error!");
                } else if (!err.response) throw new Error(err.message);
                else if (err.response.status === 502) {
                    throw new Error(err.response.statusText);
                } else if (err.response.data.error) {
                    throw new Error(err.response.data.error.message.value);
                } else {
                    throw new Error(err.response.data);
                }
            });
    }



    async function getTransactionHeads(SessionId) { // Get all transaction heads
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_TRAN_HEAD`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function getTransactionHeadsByStatus(U_status_id, SessionId) { // Get transacion head by id
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_TRAN_HEAD?$filter=U_status_id eq '${U_status_id}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function getTransactionHeadsById(Code, SessionId) { // Get transacion head by id
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_TRAN_HEAD('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function addTransactionHead(request_info, SessionId) { // Add transaction
        const {
            Name,
            U_series_id,
            U_organization_id,
            U_subgroup_id,
            U_tran_type_id,
            U_receipt_type_id,
            U_group_type_id,
            U_customer_name,
            U_paid_by,
            U_remarks,
            U_reviewed_by_id,
            U_status_id,
            U_tendered_type,
            U_amount,
            U_reference_no,
            U_official_receipt,
            U_created_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Get Code
        let codeMax = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_TRAN_HEAD/$count`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
        }).then(res => {
            return res.data
        }).catch(e => {
            throw new Error(e)
        })

        codeMax = codeMax + 1;

        // Query
        return await axiosRequest({
            method: "POST",
            link: `/b1s/v1/U_TRAN_HEAD`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Code: codeMax,
                Name,
                U_date: date_today,
                U_series_id,
                U_organization_id,
                U_subgroup_id,
                U_tran_type_id,
                U_receipt_type_id,
                U_group_type_id,
                U_customer_name,
                U_paid_by,
                U_remarks,
                U_reviewed_by_id,
                U_status_id,
                U_tendered_type,
                U_amount,
                U_reference_no,
                U_official_receipt,
                U_created_by,
                U_updated_by: U_created_by,
                U_created_time: time_today,
                U_updated_time: time_today,
                U_created_date: date_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function updateTransactionHead(request_info, SessionId) { // Update transaction
        const {
            Code,
            Name,
            U_series_id,
            U_organization_id,
            U_subgroup_id,
            U_tran_type_id,
            U_receipt_type_id,
            U_group_type_id,
            U_customer_name,
            U_paid_by,
            U_remarks,
            U_reviewed_by_id,
            U_status_id,
            U_tendered_type,
            U_amount,
            U_reference_no,
            U_official_receipt,
            U_updated_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Query
        return await axiosRequest({
            method: "PATCH",
            link: `/b1s/v1/U_TRAN_HEAD('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Name,
                U_date: date_today,
                U_series_id,
                U_organization_id,
                U_subgroup_id,
                U_tran_type_id,
                U_receipt_type_id,
                U_group_type_id,
                U_customer_name,
                U_paid_by,
                U_remarks,
                U_reviewed_by_id,
                U_status_id,
                U_tendered_type,
                U_amount,
                U_reference_no,
                U_official_receipt,
                U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function getReceiptStatus(SessionId) { // Get all receipt status
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_RECEIPT_STATUS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function getReceiptStatusByID(Code, SessionId) { // Get receipt status by id
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_RECEIPT_STATUS('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function getReceiptStatusByName(U_name, SessionId) { // Get receips status by U_name
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_RECEIPT_STATUS?$filter=U_name eq '${U_name}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function addReceiptStatus(request_info, SessionId) { // Add receipt status
        const {
            U_name,
            U_created_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Get Code
        let codeMax = await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_RECEIPT_STATUS/$count`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
        }).then(res => {
            return res.data
        }).catch(e => {
            throw new Error(e)
        })

        codeMax = codeMax + 1;

        // Query
        return await axiosRequest({
            method: "POST",
            link: `/b1s/v1/U_RECEIPT_STATUS`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Code: codeMax,
                Name: U_name,
                U_name,
                U_created_by,
                U_updated_by: U_created_by,
                U_created_time: time_today,
                U_updated_time: time_today,
                U_created_date: date_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    async function updateReceiptStatus(request_info, SessionId) { // Add receipt status
        const {
            Code,
            U_name,
            U_updated_by
        } = request_info

        // Get date and time
        const date_today = moment().format("YYYY-MM-DD");
        const time_today = moment().format("HH:mm:ss");

        // Query
        return await axiosRequest({
            method: "PATCH",
            link: `/b1s/v1/U_RECEIPT_STATUS('${Code}')`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            },
            data: {
                Name: U_name,
                U_name,
                U_updated_by,
                U_updated_time: time_today,
                U_updated_date: date_today
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }



}