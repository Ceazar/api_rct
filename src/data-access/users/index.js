//Moment
const moment = require("moment");
//Database connection
// const client = require("../database/index");

const { axiosRequest } = require("../../axios");

const users = require("./users");

const usersDB = users({ axiosRequest, moment });

module.exports = usersDB;
