module.exports = function usersDB({ axiosRequest, moment }) {
    return Object.freeze({
        login,

        checkUser,
        getUserActions,

        getModules
    });

    // Login
    async function login() {
        return await axiosRequest({
            method: "POST",
            link: "/b1s/v1/Login",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                CompanyDB: process.env.SL_CompanyDB,
                Password: process.env.SL_Password,
                UserName: process.env.SL_UserName
            }
        })
            .then(res => {
                return res.data.SessionId
            })
            .catch(err => {
                if (err.code == "ECONNREFUSED") {
                    throw new Error("Network Error!");
                } else if (!err.response) throw new Error(err.message);
                else if (err.response.status === 502) {
                    throw new Error(err.response.statusText);
                } else if (err.response.data.error) {
                    throw new Error(err.response.data.error.message.value);
                } else {
                    throw new Error(err.response.data);
                }
            });
    }

    // Get user details
    async function checkUser(username, SessionId) {
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/$crossjoin(U_USERS, U_ROLES)?$expand=U_USERS($select=Code,Name,U_role_id,U_status,U_username,U_password,U_first_name,U_middle_name,U_last_name,U_address,U_city),U_ROLES($select=Code,Name,U_name,U_status)&$filter=U_USERS/U_role_id eq U_ROLES/Code and U_USERS/U_username eq '${username}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        })
            .then(res => {
                return res
            })
            .catch(err => {
                if (err.code == "ECONNREFUSED") {
                    throw new Error("Network Error!");
                } else if (!err.response) throw new Error(err.message);
                else if (err.response.status === 502) {
                    throw new Error(err.response.statusText);
                } else if (err.response.data.error) {
                    throw new Error(err.response.data.error.message.value);
                } else {
                    throw new Error(err.response.data);
                }
            });
    }

    // Get User actions
    async function getUserActions(role_code, SessionId) {
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/$crossjoin(U_ACCESS_RIGHTS,U_ACTIONS)?$expand=U_ACCESS_RIGHTS($select=Code,Name,U_action_id,U_role_id),U_ACTIONS($select=Code,Name,U_module_id,U_desc,U_status)&$filter=U_ACCESS_RIGHTS/U_action_id eq U_ACTIONS/Code and U_ACCESS_RIGHTS/U_role_id eq '${role_code}'`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }

    // Get User actions
    async function getModules(SessionId) {
        return await axiosRequest({
            method: "GET",
            link: `/b1s/v1/U_MODULES?$select=Code,Name,U_desc`,
            headers: {
                "Content-Type": "application/json",
                Cookie: `B1SESSION=${SessionId};`
            }
        }).then(res => {
            return res.data
        }).catch(err => {
            if (err.code == "ECONNREFUSED") {
                throw new Error("Network Error!");
            } else if (!err.response) throw new Error(err.message);
            else if (err.response.status === 502) {
                throw new Error(err.response.statusText);
            } else if (err.response.data.error) {
                throw new Error(err.response.data.error.message.value);
            } else {
                throw new Error(err.response.data);
            }
        });
    }


}