const express = require("express");
const router = express.Router();

const {
    CON_listSubgroups,
    CON_addSubgroup,
    CON_updateSubgroup
} = require("../../../controllers/admin/subgroups");

const makeCallback = require("../../../express-callback");

// Subgroup
router.get("/", makeCallback(CON_listSubgroups));
router.post("/add_subgroup", makeCallback(CON_addSubgroup));
router.put("/update_subgroup", makeCallback(CON_updateSubgroup));

module.exports = router;
