const express = require("express");
const router = express.Router();

const {
    CON_listRoles,
    CON_addRole,
    CON_updateRole
} = require("../../../controllers/admin/roles");

const makeCallback = require("../../../express-callback");

// Role
router.get("/", makeCallback(CON_listRoles));
router.post("/add_role", makeCallback(CON_addRole));
router.put("/update_role", makeCallback(CON_updateRole));

module.exports = router;
