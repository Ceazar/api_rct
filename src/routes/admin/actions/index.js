const express = require("express");
const router = express.Router();

const {
    CON_listActions,
    CON_addAction,
    CON_updateAction
} = require("../../../controllers/admin/actions");

const makeCallback = require("../../../express-callback");

// Action
router.get("/", makeCallback(CON_listActions));
router.post("/add_action", makeCallback(CON_addAction));
router.put("/update_action", makeCallback(CON_updateAction));

module.exports = router;
