const express = require("express");
const router = express.Router();

const {
    CON_listReceiptTypes,
    CON_addReceiptType,
    CON_updateReceiptType
} = require("../../../controllers/admin/receipt_types");

const makeCallback = require("../../../express-callback");

// ReceiptType
router.get("/", makeCallback(CON_listReceiptTypes));
router.post("/add_receipt_type", makeCallback(CON_addReceiptType));
router.put("/update_receipt_type", makeCallback(CON_updateReceiptType));

module.exports = router;
