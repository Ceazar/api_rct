const express = require("express");
const router = express.Router();

const {
    CON_addAccessRights,
    CON_listAccessRights
} = require("../../../controllers/admin/access_rights");

const makeCallback = require("../../../express-callback");

// Action
router.get("/", makeCallback(CON_listAccessRights));
router.post("/add_access_rights", makeCallback(CON_addAccessRights));

module.exports = router;
