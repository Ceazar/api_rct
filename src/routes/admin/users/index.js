const express = require("express");
const router = express.Router();

const {
    CON_listUsers,
    CON_addUser,
    CON_updateUser
} = require("../../../controllers/admin/users");

const makeCallback = require("../../../express-callback");

// User
router.get("/", makeCallback(CON_listUsers));
router.post("/add_user", makeCallback(CON_addUser));
router.put("/update_user", makeCallback(CON_updateUser));

module.exports = router;
