const express = require("express");
const router = express.Router();

const {
    CON_listGroups,
    CON_addGroup,
    CON_updateGroup
} = require("../../../controllers/admin/groups");

const makeCallback = require("../../../express-callback");

// Group
router.get("/", makeCallback(CON_listGroups));
router.post("/add_group", makeCallback(CON_addGroup));
router.put("/update_group", makeCallback(CON_updateGroup));

module.exports = router;
