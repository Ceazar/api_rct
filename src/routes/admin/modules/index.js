const express = require("express");
const router = express.Router();

const {
    CON_listModules,
    CON_addModule,
    CON_updateModule
} = require("../../../controllers/admin/modules");

const makeCallback = require("../../../express-callback");

// Module
router.get("/", makeCallback(CON_listModules));
router.post("/add_module", makeCallback(CON_addModule));
router.put("/update_module", makeCallback(CON_updateModule));

module.exports = router;
