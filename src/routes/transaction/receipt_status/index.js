const express = require("express");
const router = express.Router();

const {
    CON_listReceiptStatus,
    CON_addReceiptStatus,
    CON_updateReceiptStatus
} = require("../../../controllers/transaction/receipt_status");

const makeCallback = require("../../../express-callback");

// ReceiptStatus
router.get("/", makeCallback(CON_listReceiptStatus));
router.post("/add_receipt_status", makeCallback(CON_addReceiptStatus));
router.put("/update_receipt_status", makeCallback(CON_updateReceiptStatus));

module.exports = router;
