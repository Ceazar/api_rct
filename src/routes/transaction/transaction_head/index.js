const express = require("express");
const router = express.Router();

const {
    CON_listTransactioHeads,
    CON_addTransactioHead,
    CON_updateTransactioHead,
    CON_getReceiptByStatus
} = require("../../../controllers/transaction/transaction_head");

const makeCallback = require("../../../express-callback");

// TransactioHead
router.get("/", makeCallback(CON_listTransactioHeads));
router.get("/get_receipt_by_status", makeCallback(CON_getReceiptByStatus));
router.post("/add_transaction_head", makeCallback(CON_addTransactioHead));
router.put("/update_transaction_head", makeCallback(CON_updateTransactioHead));

module.exports = router;
