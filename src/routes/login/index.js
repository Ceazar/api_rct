const express = require("express");
const router = express.Router();

const CON_login = require("../../controllers/login");

const makeCallback = require("../../express-callback");

router.post("/", makeCallback(CON_login));

module.exports = router;
