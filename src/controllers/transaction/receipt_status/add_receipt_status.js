module.exports = function makeAddReceiptStatus({ UC_addReceiptStatus }) {
    return async function postReceiptStatus(httpRequest) {
        try {
            const request_info = httpRequest.body;

            // Usecase
            const result = await UC_addReceiptStatus(request_info);

            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: 201,
                body: result
            };
        } catch (e) {
            // Catch error
            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: e.status ? e.status : 400,
                body: { errorMsg: e.message }
            };
        }
    };
};
