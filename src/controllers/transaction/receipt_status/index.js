// Usecases
const {
    UC_listReceiptStatus,
    UC_addReceiptStatus,
    UC_updateReceiptStatus
} = require("../../../use-cases/transaction/receipt_status");

// Controllers
const listReceiptStatus = require('./list_receipt_status');
const addReceiptStatus = require('./add_receipt_status');
const updateReceiptStatus = require('./update_receipt_status');

const CON_listReceiptStatus = listReceiptStatus({ UC_listReceiptStatus });
const CON_addReceiptStatus = addReceiptStatus({ UC_addReceiptStatus });
const CON_updateReceiptStatus = updateReceiptStatus({ UC_updateReceiptStatus });

module.exports = ({
    CON_listReceiptStatus,
    CON_addReceiptStatus,
    CON_updateReceiptStatus
})