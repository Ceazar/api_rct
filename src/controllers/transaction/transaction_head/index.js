// Usecases
const {
    UC_listTransactioHeads,
    UC_addTransactioHead,
    UC_updateTransactioHead,
    UC_getReceiptByStatus
} = require("../../../use-cases/transaction/transaction_head");

// Controllers
const listTransactioHeads = require('./list_transaction_head');
const addTransactioHead = require('./add_transaction_head');
const updateTransactioHead = require('./update_transaction_head');
const getReceiptByStatus = require('./get_receipt_by_status');

const CON_listTransactioHeads = listTransactioHeads({ UC_listTransactioHeads });
const CON_addTransactioHead = addTransactioHead({ UC_addTransactioHead });
const CON_updateTransactioHead = updateTransactioHead({ UC_updateTransactioHead });
const CON_getReceiptByStatus = getReceiptByStatus({ UC_getReceiptByStatus })

module.exports = ({
    CON_listTransactioHeads,
    CON_addTransactioHead,
    CON_updateTransactioHead,
    CON_getReceiptByStatus
})