// Use cases
const UC_login = require("../../use-cases/login");

// Function
const makeLogin = require("./login");

const CON_login = makeLogin({ UC_login });

module.exports = CON_login;
