module.exports = function makeLogin({ UC_login }) {
    return async function postLogin(httpRequest) {
        try {
            const loginInfo = httpRequest.body;
            // const browser = httpRequest.client;

            const result = await UC_login(loginInfo);

            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: 201,
                body: result
            };
        } catch (e) {
            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: e.status ? e.status : 400,
                body: { errorMsg: e.message }
            };
        }
    };
};
