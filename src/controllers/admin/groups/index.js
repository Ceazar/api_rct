// Usecases
const {
    UC_listGroups,
    UC_addGroup,
    UC_updateGroup
} = require("../../../use-cases/admin/groups");

// Controllers
const listGroups = require('./list_groups');
const addGroup = require('./add_group');
const updateGroup = require('./update_group');

const CON_listGroups = listGroups({ UC_listGroups });
const CON_addGroup = addGroup({ UC_addGroup });
const CON_updateGroup = updateGroup({ UC_updateGroup });

module.exports = ({
    CON_listGroups,
    CON_addGroup,
    CON_updateGroup
})