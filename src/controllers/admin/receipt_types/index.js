// Usecases
const {
    UC_listReceiptTypes,
    UC_addReceiptType,
    UC_updateReceiptType
} = require("../../../use-cases/admin/receipt_types");

// Controllers
const listReceiptTypes = require('./list_receipt_types');
const addReceiptType = require('./add_receipt_type');
const updateReceiptType = require('./update_receipt_type');

const CON_listReceiptTypes = listReceiptTypes({ UC_listReceiptTypes });
const CON_addReceiptType = addReceiptType({ UC_addReceiptType });
const CON_updateReceiptType = updateReceiptType({ UC_updateReceiptType });

module.exports = ({
    CON_listReceiptTypes,
    CON_addReceiptType,
    CON_updateReceiptType
})