module.exports = function makeListReceiptTypes({ UC_listReceiptTypes }) {
    return async function getReceiptTypes(httpRequest) {
        try {
            const request_info = httpRequest.body;

            // Access use case
            const result = await UC_listReceiptTypes();

            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: 200,
                body: result
            };
        } catch (e) {
            // Catch error
            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: e.status ? e.status : 400,
                body: { errorMsg: e.message }
            };
        }
    };
};
