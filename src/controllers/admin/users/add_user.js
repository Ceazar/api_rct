module.exports = function makeAddUser({ UC_addUser }) {
    return async function postUser(httpRequest) {
        try {
            const request_info = httpRequest.body;

            // Usecase
            const result = await UC_addUser(request_info);

            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: 201,
                body: result
            };
        } catch (e) {
            // Catch error
            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: e.status ? e.status : 400,
                body: { errorMsg: e.message }
            };
        }
    };
};
