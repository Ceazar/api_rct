// Usecases
const {
    UC_listUsers,
    UC_addUser,
    UC_updateUser
} = require("../../../use-cases/admin/users");

// Controllers
const listUsers = require('./list_users');
const addUser = require('./add_user');
const updateUser = require('./update_user');

const CON_listUsers = listUsers({ UC_listUsers });
const CON_addUser = addUser({ UC_addUser });
const CON_updateUser = updateUser({ UC_updateUser });

module.exports = ({
    CON_listUsers,
    CON_addUser,
    CON_updateUser
})