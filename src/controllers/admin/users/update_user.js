module.exports = function makeUpdateUser({ UC_updateUser }) {
    return async function putUser(httpRequest) {
        try {
            const request_info = httpRequest.body;

            // Usecase
            const result = await UC_updateUser(request_info);

            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: 200,
                body: result
            };
        } catch (e) {
            // Catch error
            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: e.status ? e.status : 400,
                body: { errorMsg: e.message }
            };
        }
    };
};
