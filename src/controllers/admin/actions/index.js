// Usecases
const {
    UC_listActions,
    UC_addAction,
    UC_updateAction
} = require("../../../use-cases/admin/actions");

// Controllers
const listActions = require('./list_actions');
const addAction = require('./add_action');
const updateAction = require('./update_action');

const CON_listActions = listActions({ UC_listActions });
const CON_addAction = addAction({ UC_addAction });
const CON_updateAction = updateAction({ UC_updateAction });

module.exports = ({
    CON_listActions,
    CON_addAction,
    CON_updateAction
})