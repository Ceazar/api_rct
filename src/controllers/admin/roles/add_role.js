module.exports = function makeAddRole({ UC_addRole }) {
    return async function postRole(httpRequest) {
        try {
            const request_info = httpRequest.body;

            // Usecase
            const result = await UC_addRole(request_info);

            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: 201,
                body: result
            };
        } catch (e) {
            // Catch error
            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: e.status ? e.status : 400,
                body: { errorMsg: e.message }
            };
        }
    };
};
