// Usecases
const {
    UC_listRoles,
    UC_addRole,
    UC_updateRole
} = require("../../../use-cases/admin/roles");

// Controllers
const listRoles = require('./list_roles');
const addRole = require('./add_role');
const updateRole = require('./update_role');

const CON_listRoles = listRoles({ UC_listRoles });
const CON_addRole = addRole({ UC_addRole });
const CON_updateRole = updateRole({ UC_updateRole });

module.exports = ({
    CON_listRoles,
    CON_addRole,
    CON_updateRole
})