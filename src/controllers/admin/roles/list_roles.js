module.exports = function makeListRoles({ UC_listRoles }) {
    return async function getRoles(httpRequest) {
        try {
            const request_info = httpRequest.body;

            // Access use case
            const result = await UC_listRoles();

            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: 200,
                body: result
            };
        } catch (e) {
            // Catch error
            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: e.status ? e.status : 400,
                body: { errorMsg: e.message }
            };
        }
    };
};
