// Usecases
const {
    UC_listModules,
    UC_addModule,
    UC_updateModule
} = require("../../../use-cases/admin/modules");

// Controllers
const listModules = require('./list_modules');
const addModule = require('./add_module');
const updateModule = require('./update_module');

const CON_listModules = listModules({ UC_listModules });
const CON_addModule = addModule({ UC_addModule });
const CON_updateModule = updateModule({ UC_updateModule });

module.exports = ({
    CON_listModules,
    CON_addModule,
    CON_updateModule
})