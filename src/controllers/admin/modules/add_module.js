module.exports = function makeAddModule({ UC_addModule }) {
    return async function postModule(httpRequest) {
        try {
            const request_info = httpRequest.body;

            // Usecase
            const result = await UC_addModule(request_info);

            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: 201,
                body: result
            };
        } catch (e) {
            // Catch error
            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: e.status ? e.status : 400,
                body: { errorMsg: e.message }
            };
        }
    };
};
