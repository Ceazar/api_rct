module.exports = function makeUpdateSubgroup({ UC_updateSubgroup }) {
    return async function putSubgroup(httpRequest) {
        try {
            const request_info = httpRequest.body;

            // Usecase
            const result = await UC_updateSubgroup(request_info);

            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: 200,
                body: result
            };
        } catch (e) {
            // Catch error
            return {
                headers: {
                    "Content-Type": "application/json"
                },
                status: e.status ? e.status : 400,
                body: { errorMsg: e.message }
            };
        }
    };
};
