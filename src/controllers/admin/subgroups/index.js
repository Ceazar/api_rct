// Usecases
const {
    UC_listSubgroups,
    UC_addSubgroup,
    UC_updateSubgroup
} = require("../../../use-cases/admin/subgroups");

// Controllers
const listSubgroups = require('./list_subgroups');
const addSubgroup = require('./add_subgroup');
const updateSubgroup = require('./update_subgroup');

const CON_listSubgroups = listSubgroups({ UC_listSubgroups });
const CON_addSubgroup = addSubgroup({ UC_addSubgroup });
const CON_updateSubgroup = updateSubgroup({ UC_updateSubgroup });

module.exports = ({
    CON_listSubgroups,
    CON_addSubgroup,
    CON_updateSubgroup
})