// Usecases
const {
    UC_addAccessRight,
    UC_listAccessRight
} = require("../../../use-cases/admin/access_rights");

// Controllers
const listAccessRights = require('./list_access_rights');
const addAccessRight = require('./add_access_rights');

const CON_listAccessRights = listAccessRights({ UC_listAccessRight });
const CON_addAccessRights = addAccessRight({ UC_addAccessRight });

module.exports = ({
    CON_listAccessRights,
    CON_addAccessRights
})