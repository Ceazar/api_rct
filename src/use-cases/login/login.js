// Import entity validations
const { makeUserLogin } = require("../../entities/users");

module.exports = function UserLogin({
    userDB,
    encryption,
    createToken
}) {
    return async function loginUser(request_info) {
        await makeUserLogin(request_info);
        const { username, password } = request_info

        // Get Session
        const SessionId = await userDB.login();

        // Get data of user based on username
        const user = await userDB.checkUser(username, SessionId);

        // Check if user exists
        if (user.data.value.length < 1) {
            throw new Error("User does not exist.");
        }
        if (user.data.value[0].U_USERS.U_status != 'Active') {
            throw new Error("Account is inactive.");
        }
        // if (!user.data.value[0].ROLE_STATUS) {
        //     throw new Error("User role is inactive.");
        // }

        // Check if password is correct
        if (user.data.value[0].U_USERS.U_password != password) {
            throw new Error("Incorrect password.");
        }
        // Remove password from user info
        delete user.data.value[0].U_USERS.U_password;

        // Get actions of user
        const user_actions = await userDB.getUserActions(user.data.value[0].U_ROLES.Code, SessionId);

        // Get modules 
        const modules = await userDB.getModules(SessionId);

        // Insert action into corressponding module
        for (let j = 0; j < modules.value.length; j++) {
            modules.value[j].actions = [];
            for (let i = 0; i < user_actions.value.length; i++) {
                if (modules.value[j].Code == user_actions.value[i].U_ACTIONS.U_module_id) {
                    modules.value[j].actions.push(user_actions.value[i].U_ACTIONS)
                }
            }
        }

        return {
            SessionId,
            user_details: {
                Code: user.data.value[0].U_USERS.Code,
                Name: user.data.value[0].U_USERS.name,
                U_FIRSTNAME: user.data.value[0].U_USERS.U_first_name,
                U_MIDDLENAME: user.data.value[0].U_USERS.U_middle_name,
                U_LASTNAME: user.data.value[0].U_USERS.U_last_name,
                U_STATUS: user.data.value[0].U_USERS.U_status,
                U_USERNAME: user.data.value[0].U_USERS.U_username,
                U_ADDRESS: user.data.value[0].U_USERS.U_address,
                U_CITY: user.data.value[0].U_USERS.U_city
            },
            user_role: {
                Code: user.data.value[0].U_ROLES.Code,
                Name: user.data.value[0].U_ROLES.U_name,
                U_STATUS: user.data.value[0].U_ROLES.U_status
            },
            user_actions_per_modules: modules.value
        };

    }
}