const makeLogin = require("./login");
const encryption = require("../../encryption/encrypting");
const { createToken } = require("../../middleware/token/authorization");
const { axiosRequest } = require("../../axios");

// Database
const userDB = require("../../data-access/users/index");

const UC_login = makeLogin({ userDB, encryption, createToken, axiosRequest });

module.exports = UC_login;
