// Database
const adminDB = require("../../../data-access/admin/index");

// Use cases
const listReceiptTypes = require("./list_receipt_types");
const addReceiptType = require("./add_receipt_type");
const updateReceiptType = require("./update_receipt_type")

const UC_listReceiptTypes = listReceiptTypes({ adminDB });
const UC_addReceiptType = addReceiptType({ adminDB });
const UC_updateReceiptType = updateReceiptType({ adminDB });

// Exports 
module.exports = {
    UC_listReceiptTypes,
    UC_addReceiptType,
    UC_updateReceiptType
};
