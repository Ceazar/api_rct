module.exports = function listReceiptTypes({ adminDB }) {
    return async function makeListReceiptTypes() {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Get all receipt types
        const result = await adminDB.getReceiptType(SessionId);

        if (!result.value.length) {
            return 'No data found';
        } else {
            return { receipt_types: result.value };
        }
    };
};
