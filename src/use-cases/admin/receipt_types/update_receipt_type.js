const { makeUpdateReceiptType } = require("../../../entities/admin/receipt_types");

module.exports = function updateReceiptTypes({ adminDB }) {
    return async function makeUpdateReceiptTypes(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeUpdateReceiptType(request_info, adminDB, SessionId);

        // Update receipt types
        await adminDB.updateReceiptType(request_info, SessionId);

        return request_info;
    };
};
