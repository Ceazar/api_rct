const { makeAddReceiptType } = require("../../../entities/admin/receipt_types")

module.exports = function addReceiptTypes({ adminDB }) {
    return async function makeAddReceiptTypes(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeAddReceiptType(request_info, adminDB, SessionId);

        // Insert receipt types
        const result = await adminDB.addReceiptType(request_info, SessionId);

        return result;
    };
};
