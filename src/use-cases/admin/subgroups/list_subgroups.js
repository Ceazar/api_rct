module.exports = function listSubgroups({ adminDB }) {
    return async function makeListSubgroups() {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Get all receipt types
        const result = await adminDB.getSubgroups(SessionId);

        if (!result.value.length) {
            return 'No data found';
        } else {
            return { subgroups: result.value };
        }
    };
};
