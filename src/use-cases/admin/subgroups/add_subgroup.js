const { makeAddSubgroup } = require("../../../entities/admin/subgroups")

module.exports = function addSubgroups({ adminDB }) {
    return async function makeAddSubgroups(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeAddSubgroup(request_info, adminDB, SessionId);

        // Insert receipt types
        const result = await adminDB.addSubgroup(request_info, SessionId);

        return result;
    };
};
