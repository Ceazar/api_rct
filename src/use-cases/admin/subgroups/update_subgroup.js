const { makeUpdateSubgroup } = require("../../../entities/admin/subgroups");

module.exports = function updateSubgroups({ adminDB }) {
    return async function makeUpdateSubgroups(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeUpdateSubgroup(request_info, adminDB, SessionId);

        // Update receipt types
        await adminDB.updateSubgroup(request_info, SessionId);

        return request_info;
    };
};
