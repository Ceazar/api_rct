// Database
const adminDB = require("../../../data-access/admin/index");

// Use cases
const listSubgroups = require("./list_subgroups");
const addSubgroup = require("./add_subgroup");
const updateSubgroup = require("./update_subgroup")

const UC_listSubgroups = listSubgroups({ adminDB });
const UC_addSubgroup = addSubgroup({ adminDB });
const UC_updateSubgroup = updateSubgroup({ adminDB });

// Exports 
module.exports = {
    UC_listSubgroups,
    UC_addSubgroup,
    UC_updateSubgroup
};
