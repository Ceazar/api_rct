// Database
const adminDB = require("../../../data-access/admin/index");

// Use cases
const listUsers = require("./list_users");
const addUser = require("./add_user");
const updateUser = require("./update_user")

const UC_listUsers = listUsers({ adminDB });
const UC_addUser = addUser({ adminDB });
const UC_updateUser = updateUser({ adminDB });

// Exports 
module.exports = {
    UC_listUsers,
    UC_addUser,
    UC_updateUser
};
