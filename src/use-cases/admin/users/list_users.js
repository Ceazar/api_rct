module.exports = function listUsers({ adminDB }) {
    return async function makeListUsers() {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Get all users
        const result = await adminDB.getAllUsers(SessionId);

        if (!result.value.length) {
            return 'No data found';
        } else {
            return { users: result.value };
        }
    };
};
