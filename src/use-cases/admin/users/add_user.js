const { makeAddUser } = require("../../../entities/admin/users")

module.exports = function addUsers({ adminDB }) {
    return async function makeAddUsers(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeAddUser(request_info, adminDB, SessionId);

        // Insert user
        console.log('asd');
        const result = await adminDB.addUser(request_info, SessionId);

        return result;
    };
};
