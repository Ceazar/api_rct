const { makeUpdateUser } = require("../../../entities/admin/users");

module.exports = function updateUsers({ adminDB }) {
    return async function makeUpdateUsers(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeUpdateUser(request_info, adminDB, SessionId);

        // Update user
        const result = await adminDB.updateUser(request_info, SessionId);

        return result;
    };
};
