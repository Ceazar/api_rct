const { makeAddModule } = require("../../../entities/admin/modules")

module.exports = function addModules({ adminDB }) {
    return async function makeAddModules(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeAddModule(request_info, adminDB, SessionId);

        // Insert module
        const result = await adminDB.addModule(request_info, SessionId);

        return result
    };
};
