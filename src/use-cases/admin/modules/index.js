// Database
const adminDB = require("../../../data-access/admin/index");

// Use cases
const listModules = require("./list_modules");
const addModule = require("./add_module");
const updateModule = require("./update_module")

const UC_listModules = listModules({ adminDB });
const UC_addModule = addModule({ adminDB });
const UC_updateModule = updateModule({ adminDB });

// Exports 
module.exports = {
    UC_listModules,
    UC_addModule,
    UC_updateModule
};
