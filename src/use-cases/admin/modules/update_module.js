const { makeUpdateModule } = require("../../../entities/admin/modules");

module.exports = function updateModules({ adminDB }) {
    return async function makeUpdateModules(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeUpdateModule(request_info, adminDB, SessionId)

        // Update module
        const result = await adminDB.updateModule(request_info, SessionId);

        return result
    };
};
