module.exports = function listModules({ adminDB }) {
    return async function makeListModules() {

        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Get all modules
        const result = await adminDB.getAllModules(SessionId);

        if (!result.value.length) {
            return 'No data found';
        } else {
            return { modules: result.value };
        }
    };
};
