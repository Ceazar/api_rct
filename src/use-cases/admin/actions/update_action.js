const { makeUpdateAction } = require("../../../entities/admin/actions");

module.exports = function updateActions({ adminDB }) {
    return async function makeUpdateActions(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeUpdateAction(request_info, adminDB, SessionId);

        // Update action
        const result = await adminDB.updateAction(request_info, SessionId);

        return result;
    };
};
