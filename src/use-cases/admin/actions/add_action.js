const { makeAddAction } = require("../../../entities/admin/actions")

module.exports = function addActions({ adminDB }) {
    return async function makeAddActions(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeAddAction(request_info, adminDB, SessionId);
        // Insert action
        const result = await adminDB.addAction(request_info, SessionId);

        return result;
    };
};
