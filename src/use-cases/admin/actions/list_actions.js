module.exports = function listActions({ adminDB }) {
    return async function makeListActions() {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Get all actions
        const result = await adminDB.getAllActions(SessionId);

        if (!result.value.length) {
            return 'No data found';
        } else {
            return { actions: result.value };
        }
    };
};
