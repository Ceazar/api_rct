// Database
const adminDB = require("../../../data-access/admin/index");

// Use cases
const listActions = require("./list_actions");
const addAction = require("./add_action");
const updateAction = require("./update_action")

const UC_listActions = listActions({ adminDB });
const UC_addAction = addAction({ adminDB });
const UC_updateAction = updateAction({ adminDB });

// Exports 
module.exports = {
    UC_listActions,
    UC_addAction,
    UC_updateAction
};
