const { makeAddAR } = require("../../../entities/admin/access_rights")

module.exports = function addActions({ adminDB }) {
    return async function makeAddActions(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeAddAR(request_info);

        const { U_role_id, actions, U_created_by } = request_info;

        const access_rights = await adminDB.getARPerRole(U_role_id, SessionId);
        let onActions = [], addedActions = [], deletedAction = [], inactiveAR = [];

        // Get all action ID
        for (let m = 0; m < access_rights.value.length; m++) {
            if (access_rights.value[m].U_ACCESS_RIGHTS.U_status == "Inactive") {
                if (actions.includes(access_rights.value[m].U_ACTIONS.Code)) {
                    inactiveAR.push(access_rights.value[m].U_ACCESS_RIGHTS.Code)
                }
            }
            onActions.push(access_rights.value[m].U_ACTIONS.Code);

        }

        // Get Added actions
        for (let k = 0; k < actions.length; k++) {
            if (!onActions.includes(actions[k])) {
                addedActions.push(actions[k])
            }
        }

        // Get Deleted actions
        for (let l = 0; l < onActions.length; l++) {
            if (!actions.includes(onActions[l])) {
                if (access_rights.value[l].U_ACCESS_RIGHTS.U_status == "Active") {
                    await adminDB.patchAccessRights(onActions[l], 'Inactive', U_created_by, SessionId)
                }
            }
        }

        // Inser new access rights
        for (let n = 0; n < addedActions.length; n++) {
            await adminDB.addAccessRights(U_role_id, addedActions[n], U_created_by, SessionId);
        }

        // Update inactive access rights
        for (let o = 0; o < inactiveAR.length; o++) {
            await adminDB.patchAccessRights(inactiveAR[o], 'Active', U_created_by, SessionId)
        }


    };
};
