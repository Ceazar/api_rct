// Database
const adminDB = require("../../../data-access/admin/index");

// Use cases
const listAccessRights = require("./list_access_rights");
const addAccessRights = require("./add_access_rights");

const UC_listAccessRight = listAccessRights({ adminDB });
const UC_addAccessRight = addAccessRights({ adminDB });

// Exports 
module.exports = {
    UC_listAccessRight,
    UC_addAccessRight
};
