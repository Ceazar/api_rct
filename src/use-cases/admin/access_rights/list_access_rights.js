module.exports = function listAccessRights({ adminDB }) {
    return async function makeListAccessRights() {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Get all modules
        const result = await adminDB.getAllAccessRights(SessionId);

        if (!result.value.length) {
            return 'No data found';
        } else {
            return { access_rights: result.value };
        }
    };
};
