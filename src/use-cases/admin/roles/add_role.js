const { makeAddRole } = require("../../../entities/admin/roles")

module.exports = function addRoles({ adminDB }) {
    return async function makeAddRoles(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeAddRole(request_info, adminDB, SessionId);

        // Insert role
        const result = await adminDB.addRole(request_info, SessionId);

        return result
    };
};
