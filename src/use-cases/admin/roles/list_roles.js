module.exports = function listRoles({ adminDB }) {
    return async function makeListRoles() {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Get all modules
        const result = await adminDB.getAllRoles(SessionId);

        if (!result.value.length) {
            return 'No data found';
        } else {
            return { roles: result.value };
        }
    };
};
