// Database
const adminDB = require("../../../data-access/admin/index");

// Use cases
const listRoles = require("./list_roles");
const addRole = require("./add_role");
const updateRole = require("./update_role")

const UC_listRoles = listRoles({ adminDB });
const UC_addRole = addRole({ adminDB });
const UC_updateRole = updateRole({ adminDB });

// Exports 
module.exports = {
    UC_listRoles,
    UC_addRole,
    UC_updateRole
};
