const { makeUpdateRole } = require("../../../entities/admin/roles");

module.exports = function updateRoles({ adminDB }) {
    return async function makeUpdateRoles(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeUpdateRole(request_info, adminDB, SessionId)

        // Update role
        const result = await adminDB.updateRole(request_info, SessionId);

        return result
    };
};
