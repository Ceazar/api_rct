const { makeUpdateGroup } = require("../../../entities/admin/groups");

module.exports = function updateGroups({ adminDB }) {
    return async function makeUpdateGroups(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeUpdateGroup(request_info, adminDB, SessionId);

        // Update receipt types
        await adminDB.updateGroup(request_info, SessionId);

        return request_info;
    };
};
