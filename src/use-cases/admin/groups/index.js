// Database
const adminDB = require("../../../data-access/admin/index");

// Use cases
const listGroups = require("./list_groups");
const addGroup = require("./add_group");
const updateGroup = require("./update_group")

const UC_listGroups = listGroups({ adminDB });
const UC_addGroup = addGroup({ adminDB });
const UC_updateGroup = updateGroup({ adminDB });

// Exports 
module.exports = {
    UC_listGroups,
    UC_addGroup,
    UC_updateGroup
};
