module.exports = function listGroups({ adminDB }) {
    return async function makeListGroups() {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Get all receipt types
        const result = await adminDB.getGroups(SessionId);

        if (!result.value.length) {
            return 'No data found';
        } else {
            return { groups: result.value };
        }
    };
};
