const { makeAddGroup } = require("../../../entities/admin/groups")

module.exports = function addGroups({ adminDB }) {
    return async function makeAddGroups(request_info) {
        // Login SAP
        const SessionId = await adminDB.SAPlogin();

        // Validation
        await makeAddGroup(request_info, adminDB, SessionId);

        // Insert receipt types
        const result = await adminDB.addGroup(request_info, SessionId);

        return result;
    };
};
