const { makeAddReceiptStatus } = require("../../../entities/transaction/receipt_status")

module.exports = function addActions({ transactionDB }) {
    return async function makeAddActions(request_info) {
        // Login SAP
        const SessionId = await transactionDB.SAPlogin();

        // Validation
        await makeAddReceiptStatus(request_info, transactionDB, SessionId);

        // Insert receipt status
        const result = await transactionDB.addReceiptStatus(request_info, SessionId);

        return result;
    };
};
