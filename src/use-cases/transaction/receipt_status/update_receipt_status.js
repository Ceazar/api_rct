const { makeUpdateReceiptStatus } = require("../../../entities/transaction/receipt_status");

module.exports = function updateReceiptStatuss({ transactionDB }) {
    return async function makeUpdateReceiptStatuss(request_info) {
        // Login SAP
        const SessionId = await transactionDB.SAPlogin();

        // Validation
        await makeUpdateReceiptStatus(request_info, transactionDB, SessionId);

        // Update receipt status
        await transactionDB.updateReceiptStatus(request_info, SessionId);

        return request_info;
    };
};
