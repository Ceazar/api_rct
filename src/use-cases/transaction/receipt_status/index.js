// Database
const transactionDB = require("../../../data-access/transaction/index");

// Use cases
const listReceiptStatus = require("./list_receipt_status");
const addReceiptStatus = require("./add_receipt_status");
const updateReceiptStatus = require("./update_receipt_status")

const UC_listReceiptStatus = listReceiptStatus({ transactionDB });
const UC_addReceiptStatus = addReceiptStatus({ transactionDB });
const UC_updateReceiptStatus = updateReceiptStatus({ transactionDB });

// Exports 
module.exports = {
    UC_listReceiptStatus,
    UC_addReceiptStatus,
    UC_updateReceiptStatus
};
