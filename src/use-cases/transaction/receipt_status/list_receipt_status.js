module.exports = function listReceiptStatus({ transactionDB }) {
    return async function makeListReceiptStatus() {
        // Login SAP
        const SessionId = await transactionDB.SAPlogin();

        // Get all transaction heads
        const result = await transactionDB.getReceiptStatus(SessionId);

        if (!result.value.length) {
            return 'No data found';
        } else {
            return { receipt_status: result.value };
        }
    };
};
