module.exports = function listTransactioHeads({ transactionDB }) {
    return async function makeListTransactioHeads() {
        // Login SAP
        const SessionId = await transactionDB.SAPlogin();

        // Get all transaction heads
        const result = await transactionDB.getTransactionHeads(SessionId);

        if (!result.value.length) {
            return 'No data found';
        } else {
            return { transactions: result.value };
        }
    };
};
