// Database
const transactionDB = require("../../../data-access/transaction/index");

// Use cases
const listTransactioHeads = require("./list_transaction_head");
const addTransactioHead = require("./add_transaction_head");
const updateTransactioHead = require("./update_transaction_head");
const getReceiptByStatus = require("./get_receipt_by_status");

const UC_listTransactioHeads = listTransactioHeads({ transactionDB });
const UC_addTransactioHead = addTransactioHead({ transactionDB });
const UC_updateTransactioHead = updateTransactioHead({ transactionDB });
const UC_getReceiptByStatus = getReceiptByStatus({ transactionDB })

// Exports 
module.exports = {
    UC_listTransactioHeads,
    UC_addTransactioHead,
    UC_updateTransactioHead,
    UC_getReceiptByStatus
};
