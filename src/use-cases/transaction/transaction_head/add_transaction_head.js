const { makeAddTransactioHead } = require("../../../entities/transaction/tran_head")

module.exports = function addActions({ transactionDB }) {
    return async function makeAddActions(request_info) {
        // Login SAP
        const SessionId = await transactionDB.SAPlogin();

        // Validation
        await makeAddTransactioHead(request_info, transactionDB, SessionId);

        // Insert transaction head
        const result = await transactionDB.addTransactionHead(request_info, SessionId);

        return result;
    };
};
