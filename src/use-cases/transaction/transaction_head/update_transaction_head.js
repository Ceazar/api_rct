const { makeUpdateTransactioHead } = require("../../../entities/transaction/tran_head");

module.exports = function updateTransactioHeads({ transactionDB }) {
    return async function makeUpdateTransactioHeads(request_info) {
        // Login SAP
        const SessionId = await transactionDB.SAPlogin();

        // Validation
        await makeUpdateTransactioHead(request_info, transactionDB, SessionId);

        // Update transaction head
        await transactionDB.updateTransactionHead(request_info, SessionId);

        return request_info;
    };
};
