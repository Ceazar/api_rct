const { makeGetReceiptByStatus } = require("../../../entities/transaction/tran_head");

module.exports = function listReceiptByStatus({ transactionDB }) {
    return async function makeReceiptByStatus(request_info) {

        // Login SAP
        const SessionId = await transactionDB.SAPlogin();

        // Validation
        const checkStatus = await makeGetReceiptByStatus(request_info, transactionDB, SessionId);

        // Get all transaction heads
        const result = await transactionDB.getTransactionHeadsByStatus(checkStatus.getStatusId(), SessionId);

        if (!result) {
            return 'No data found';
        } else {
            return { receipts: result.value };
        }
    };
};
