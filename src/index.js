const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
require("dotenv").config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

const notFound = require("./controllers/not_found/not_found");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.get("/", (req, res) => {
    res.send("RCT API");
});

// Login
app.use("/login", require("./routes/login"));

// ----Admin------
// Modules
app.use("/admin/modules", require("./routes/admin/modules"));
// Roles
app.use("/admin/roles", require("./routes/admin/roles"));
// Actions
app.use("/admin/actions", require("./routes/admin/actions"));
// Access Rights
app.use("/admin/access_rights", require("./routes/admin/access_rights"));
// Users
app.use("/admin/users", require("./routes/admin/users"));
// Receipt Type
app.use("/admin/receipt_types", require("./routes/admin/receipt_types"));
// Groups
app.use("/admin/groups", require("./routes/admin/groups"));
// Subgroups
app.use("/admin/subgroups", require("./routes/admin/subgroups"));


// ----Transaction----
// Transaction head
app.use("/transaction/transaction_head", require("./routes/transaction/transaction_head"));
// Receipt Status
app.use("/transaction/receipt_status", require("./routes/transaction/receipt_status"));





app.use(async (req, res) => {
    data = await notFound();
    res.status(data.status).send(data.body);
});

app.use(function (err, req, res, next) {
    res.status(500).send({ errorMsg: "Unkown error!", ...err });
});

var port = process.env.port || 4004;
app.listen(port, console.log(`Server started at port ${port}`));
